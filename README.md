# Expanded Matter

"Matter expands on its own free will."

This Vintage Story mod library adds new variants and types to the base game materials.
To be used by other mods, contains no end-user functionality in itself.

## More Information

You can find more information and pretty pictures at our [Wiki](https://gitlab.com/codesmiths/vs_expanded_matter/-/wikis/home).

We hope you enjoy our work.

~Phiwa & Tels

# Download

* The mod From [Official Vintage Story ModDB](https://mods.vintagestory.at/em)
* The source code can be found on [Gitlab](https://gitlab.com/codesmiths/vs_expanded_matter/-/releases)

# Installation

This mod should work in existing worlds. If in doubt, please create a new world.

  <u>**Make a backup of your savegame and world before trying this mod!**</u>

# Languages

* English (100%)
* German (100%)
* Japanese (100%)
* Portuguese (50%)
* Spanish (100%)
* Ukrainian (100%)

All other languages are more or less missing completely.
If you would like to translate this mod into other languages, please [contact us](https://gitlab.com/codesmiths/vs_expanded_matter/-/wikis/Contact).

# Changes and Roadmap

Please see the [Changelog](Changelog) for the changes in the latest release.

If you have any ideas, or know about features that already done in another mod, then we
love [to hear from you](https://gitlab.com/codesmiths/vs_bricklayers/-/wikis/Contact).

# Signature key

All our releases are signed using PGP (GnuPG) and their integrity can be verified by using the public key as published
[on the wiki](https://gitlab.com/codesmiths/vs_bricklayers/-/wikis/Signing-Key).

