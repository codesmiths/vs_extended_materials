This mod would have not been possible with the help of countless other people. We stand on the shoulders of giants!

Thank you,

~Tels & Phiwa

# Contributors

Without you, this mod would not have been possible - thank you!

## Thank you

First and foremost a big thank you goes to **Saraty**, **Tyron** and all the other
team members for creating such a great game!

### In Memory Of

* **Hypi** aka *hypnotique* - Godspeed on your eternal travel to the best of all places! - November 2024

### Translators

* Brazilian Portuguese: **William Tarry (Dracomancer)**
* French: **Drakker**
* English advisors: **Ashantin**, **jjallie1**
* German: **Tels**, **Phiwa**
* Italian: **x_Yota_x**, **Nahuel-Campos**
* Japanese: **Macoto Hino**
* Russian: **Craluminum**
* Spanish: **Darce**, **ElectroNikkel**
* Spanish/Latin American: **ElectroNikkel**
* Ukrainian: **DeanBro**, **justOmi**

### Testing, Feedback and Bugreporting

* Too many people to list them all :)

### Modding help

* All the people answering (our and others) questions, on the modding-help discord channel or elsewere: You are awesome!
Especially:
  + **Asraiel**
  + **Capsup**
  + **DArkHekRoMaNT**
  + **JapanHasRice**
  + **l33tmaan**
  + **Radfast**
  + **SpearAndFang**
