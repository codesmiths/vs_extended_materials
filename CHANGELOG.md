# v3.1.1 - 2024-03-14 - Fixes

## Bugfixes

* #136 - Remove duplicate entry in en.json (Thanx, DejFidOFF)
* #134 - Fix harmless warning about non-existing em:powdered-ore-flint

## Compatibility With Other Mods

* #138 - Disable vinegar recipe when Expanded Foods is installed
* #137 - Remove Wildcraft: Fruit vinegar compatibility recipe

# v3.1.0 - 2024-03-01 - Ore Panning

## New Features

* #131 - Crush flint into crushed flint, ratio 1:1.5 (with config setting from 1x to 3.x)
* #130 - Add rare ores to loot tables [1]
* #129 - Add rare ores to panning [1]

## Ore list

1: corundum, fluorite, kernite, phosphorite, rhodochrosite, uranium

# Tweaking and Balancing

* #132 - Increase coke burn time from 40 to 80 seconds
* #131 - Crushed flint calcinates 30% faster than raw flint

## Translations

* #133 - Updated Ukrainian translation, thanx justOmi!

# v3.0.1 - 2024-02-15 - Small Fixes

## Compatibility With Other Mods

* #127 - Disable vinegar patches when Art of Cooking is loaded

## New Features

* #128 - Grind crushed alum into alum powder, use alum powder for diluted alum

# v3.0.0 - 2024-01-18 - VS 1.20 Edition

## Bugfixes

* #122 - Fix transforms for powders dropped on ground

## Compatibility with VS 1.20

* #116 - Grinding powdered charcoal gives 2x powdered-ore-coal
* #116 - Terra Preta crafting needs 2x as much coal to match VS
* #113 - Fix recipes and references for VS 1.20:
  - fix cooking glue with `powedered-ore-coal`
  - fix crafting terra preta with crushed and powdered coal
  - fix references to powdered sulfur and powdered borax
  - remove `em:powdered-ore-cinnabar` and `em:powdered-ore-lapislazuli`
  - rename `game:powdered-lapislazuli` to Ultramarine
  - rename `game:powdered-cinnabar` to Vermillion

## New Features

* #126 - Craft vinegar by sealing fruit mash in a barrel
* #125 - Craft white lead powder by cooking lead+vinegar+tannin+compost
* #118 - Crush iron ingots/nuggets/bits, powder crushed iron, powdered iron rusts into oxide
* #118 - Allow usage of powdered hematite in crafting new bomb types
* #117 - Rose quartz can be crushed into rhodochrosite
* #114 - Add recipes for crafting diluted cassiterite and chromite from powders
* #112 - Add caustic potash (liquid), craft Yellowcake from it
* #89 - Add magnesium hydroxide and magnesia (magnesium oxide)

## Tweaks and Balancing

* #124 - Powdered lead is cooked to powdered galena in a cooking pot
  Background: Items with a conversion temp. cannot be used in cooking recipes

## Compatibility with Other Mods

* #126 - Craft vinegar from Wildcraft: Fruit mash

## Translations

* #120 - Updated Japanese translation, thanx Macoto Hino!
* Add a few more selected translations for other languages

# v2.7.0 - 2024-08-06 - Diluted Saltpeter

## New Features

* #106 - Added diluted saltpeter
* #106 - Curing of meat and vegetables with saltpeter brine can be enabled via config

## Translations

* #111 - Added Ukrainian translation, thanx DeanBro!

# v2.6.3 - 2024-07-06 - Burn, Graphite, Burn

## New Features

* #109 - 22 more powdered items can be used as pigments (signs, chests, etc.)
* #108 - Powdered graphite can be used as fuel

# v2.6.2 - 2024-06-29 - Rock Ash

## New Features

* #107 - Add inorganic (rock) ash (disabled by default)

# v2.6.1 - 2024-06-17 - Dyes Galore

## New Features

* #102 - optional config lib support

## Compatibility With Other Mods

* #101 - Move dye patches from Bricklayers to Expanded Matter
* #100 - Patch sealhours for Wildcraft (Trees and Fruits) dyes

# v2.6.0 - 2024-05-15 - Legacy Removed

## Legacy Removal

* #97 - Remove legacy items: powdered-ore-bauxite, powdered-ore-granite, crushed-ore-granite
        These are mapped to powdered-stone-X and crushed-stone-X upon world load

## Compatibility With VS 1.19

* #96 - Allow crushed coal to be used in Terra Preta crafting
* #95 - Remove obsolete fpHandTransform from JSONs
* #95 - Require VS 1.19.7 to get fixes for transforms

# v2.5.2 - 2024-03-24 - More Powdered Coal

## Compatibility With Other Mods

* #94 - Compatibility with Ancient Tools: craft empty pitch pots

# v2.5.1 - 2024-03-01 - Powdered Coal

## Compatibility With VS 1.19.4

* #91 - Powdered charcoal can be turned into powdered coal
* #91 - Powdered charcoal can be used to craft blasting powder
* #91 - Powdered coal can be used to craft glue

# v2.5.0 - 2024-01-26 - 1.19 Dejank

## Optimizations

* #81 - Optimize crushed/powdered 3D models by re-using incontainer textures

## New Features

* #86 - Blasting powder is crafted with crushed or powdered coal (from Bricklayers)
* #83 - Metal parts can be crushed into 4 pieces (vs. 5 from cutting with chisel)

## Compatibility With VS 1.19

* #90 - Fix: metal scraps could no longer be pulverized into crushed rusty metal
* #85 - Use our own crushed.json shape instead of the missing game one
* #84 - Change grindedStack to groundStack for VS 1.19 compatibility

# v2.4.1 - 2023-09-26 - Grinding vs. Ground

## Bugfixes

* #80 - Grinding some ores like bauxite was broken due to VS changes (Thanx, BillyGalbreath!)

# v2.4.0 - 2023-09-22 - Stones and Sand

## Features

* #69 - Add crushed and powdered stones for the mod *Not Enough Sand*

## Translations

* #79 - Add Spanish/Latin American translation (Thanx, ElectroNikkel)
* #79 - Updated Spanish translation (Thanx, ElectroNikkel)
* #69 - Updated Japanese translation (Thanx, Macoto Hino!)

# v2.3.0 - 2023-08-09 - Rare Metals

## Features

* #77 - Allow smelting of crushed metal ores (Thanx Alatyr!)
* #68 - Allow smelting of uranium bits, nuggets and crushed uranium
* #68 - Allow crafting of uranium plates from uranium ingots

## New features

* #72 - Large temporal gears can be crushed into temporal pieces

## Vanilla Tweaks

* #72 - Large temporal gears emit a faint glow
* #68 - Unhide chromium and uranium bits, ingots and plates from handbook

# v2.2.2 - 2023-07-14 - Pigment Fixes

## Bugfixes

* #70 - Fix: malachite nuggets had no pigment anymore

# v2.2.1 - 2023-06-05 - Smelting Fixes

## Bugfixes

* #66 - Crushed ores did not go into the crucible and could thus not be smelted
* #65 - Fix missing handbook entry for zinc oxide
* #65 - Update Japanese translation

# v2.2.0 - 2023-05-24 - More Metals

## Vanilla Tweaks

* #61 - Red dye now made with vermillion (powdered cinnabar) instead of crushed cinnabar

## New features

* #55 - Add manganese oxide (MnO) and manganese dioxide (MnO₂) powder

# v2.1.3 - 2023-05-21 - Hotfixes

## Bugfixes

* #58 - Add Ceramos compatibility: make lead frit craftable again
* #56 - Fix: silver nuggets could not be crushed

# v2.1.2 - 2023-05-18 - Nickel Fixes

## Compatibilitiy with 1.18.3

* #51 - Remove unnec. patches for cupronickel and electrum bits and ingots

## New features

* #52 - Add crushed nickel, nickel bits and ingots can be crushed

# v2.1.1 - 2023-05-09 - Electrum Fixes

## Bugfixes

* #49 - Electrum: fix langkey, ingots and workitems, add plate recipe

# v2.1.0 - 2023-05-08 - Expanded Metals

## Bugfixes

* #33 - Fix: Crushed silver could not be obtained

## Vanilla Tweaks

* #48 - Cupronickel bits can be smelted into ingots
* #48 - Metal parts can be cut into cupronickel bits
* #46 - Electrum bits can be smelted into ingots

## New features

* #46 - Add crushed and powdered electrum
* #27 - Add crushed and powdered graphite

## Balancing

* #33 - Crushing ingots produces 20 crushed pieces

## Translations

* Add some missing language entries
* Fix typos

# v2.0.0 - 2023-04-21 - Powders and Ores

## Vanilla Tweaks

* #36 - Add descriptions to crushed and powdered metals and ores
* #32 - Crushed chromite can be used for tier 3 refractory bricks
* #29 - Dye can be crafted using the new powdered materials
* #26 - Now visible in barrels: powdered borax, powdered sulfur
* #26 - Kernite can be turned into borax
* #17 - Add pigment colors and names for: chromite, anthracite, fluorite
* #1 - Improve handbook entry for: corundum

## Bugfixes

* #43 - Crushed bauxite could not be ground into Powdered bauxite
* #41 - Bone ash visible as fertilizer on farm land
* #40 - Supress handbook entries for non-existing descriptions
* #37 - Handbook entries for crushed items appeared twice
* #33 - Fix: Crushing pentlandite ore yields same amount as crushing nuggets
* #33 - Fix: Impossible to crush some nuggets: cassiterite, chromite, ilmenite

## New features

* #42 - Crushed galena and sphalerite ores can be smelted down
* #39 - Most crushed metals can be smelted down
* #38 - For 1.18: Add crushed and powdered cupronickel
* #26 - Kernite can be turned into borax
* #1 - Add crushed luminous ores: phosphorite, uranium
* #1 - Add crushed luminous metals: uranium
* #1 - Add crushed ores: coal, corundum, galena, granite, hematite, limonite, magnetite, malachite, pentlandite, rhodochrosite, sphalerite
* #1 - Add crushed metals: copper, gold, lead, rusty, silver, tin, titanium, zinc
* #1 - Add powdered luminous ores: fluorite, phosphorite, redphosphorus, whitephoshorus
* #1 - Add powdered ores: bauxite, boneash, cassiterite, cinnabar, chromite, chromiumoxide, coal, corundum, galena, granite, hematite, ilmenite, kernite, lapislazuli, limonite, litharge, leadoxide, magnetite, malachite, massicot, olivine, pentlandite, rhodochrosite, quartz, sphalerite, whitelead, yellowcake, zinc oxide
* #1 - Add powdered metals: copper, gold, lead, nickel, rusty, silver, tin, uranium, zinc

## Translations

* Started Chinese translation
* Started Polish translation
* Started Swedish translation

## Testsuite

* #1 - Testsuite properly handles "skipVariants"
* #1 - Improve testsuite to check for missing handbook entries

# v1.1.2 - 2023-02-19 - Tweaks

## Balancing

* #19 - Increase output of ash-sludge by a factor of two

## Translations

* #24 - Add Japanese translation (Thanx, Macoto Hino!)
* #22 - Add Italian translation (Thanx x_Yota_x & Nahuel-Campos)

# v1.1.1 - 2023-01-30 - Tweaks

## Balancing

* #18 - increase sealing and drying times of ash

# v1.1.0 - 2022-11-13 - Ashes

## New features

* #14 - Add crafting of plant or wood ash and ash sludge. Disabled by default.
* #13 - Add French translation provided by Drakker

# v1.0.1 - 2022-09-18 - First Version

* #10 - Mark glass patches as server-side
* #9 - Add translation to creative tab names, put crushed temporal into Luminous tab

# v1.0.0 - 2022-08-31 - First Version

## New features

* #8 - Add spanish translation by Darce
* #2 - Add crushed temporal pieces and temporal dust
