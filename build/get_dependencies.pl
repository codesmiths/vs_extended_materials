#!/usr/bin/perl -w

# Load modinfo.json, parse the dependencies and generate download links
# for all mods except the base game "mods".

# If passed a folder name, will download the mods into that folder.

use warnings;
use strict;
use JSON;
use Encode qw/ encode /;
use WWW::Curl::Easy;
use SemVer;
use File::Temp qw/ tempfile /;
use File::Copy qw/ move /;

use constant VERSION		=> '0.0.2';

use constant API_BASE_URL	=> 'https://mods.vintagestory.at/api/';
use constant API_FILE_URL	=> 'https://mods.vintagestory.at/download?fileid=';

use constant FILE_CACHE_URL	=> 'http://bloodgate.com/mirrors/vs/mods/byid/';

use constant TEMP_DIR		=> '/tmp/';

use constant USER_AGENT		=> "ModDependencyDownloader/v" . VERSION;

use constant DEBUG		=> 1;
use constant DIE_ON_ERRORS	=> 1;

# Subroutine definitions
sub parse_json($);
sub parse_json_file($);
sub api_load_modinfo($);
sub api_load_file($ $ $);
sub log_debug($);
sub log_error($);
sub log_fatal($);
sub log_warn($);

my $modinfo_name = shift;
my $destination = shift;
log_fatal("Destination $destination is not a folder: $!") if $destination && !-d $destination;

my $modinfo = parse_json_file( $modinfo_name );

log_fatal("Could not parse '$modinfo_name': " . $modinfo)
	if !ref $modinfo;

# use Data::Dumper; print Dumper($modinfo),"\n";

for my $name (keys %{$modinfo->{dependencies}})
  {
  next if $name =~ /^(survival|game)$/;

  my $min_ver = SemVer->new( $modinfo->{dependencies}->{$name} );

  log_debug("Looking at $name (min: v$min_ver)");

  my $info = api_load_modinfo($name);

  if (!ref($info))
    {
    # there was an error retrieving the info
    log_fatal("Could not retrieve mod info: $info");
    next;
    }

  my $sc = $info->{statuscode} // '';
  if ($sc ne '200')
    {
    log_fatal("API did not return status code 200, but $sc");
    next;
    }

  if (ref ($info->{mod}) ne 'HASH')
    {
    log_fatal('API did not return mod info under "mod"');
    next;
    }

  $info = $info->{mod};
  if (ref ($info->{releases}) ne 'ARRAY')
    {
    log_fatal("No releases for mod $name");
    next;
    }

  # this is the smallest so far
  my $min_release_ver = SemVer->new('0.0.0');
  my $use_release = undef;

  # Go through all releases
  # use the latest one that is greater than the needed one
  RELEASE:
  for my $release (@{ $info->{releases} })
    {
    my $rel_version = SemVer->new( $release->{modversion} // '0.0.0' );
    if ($rel_version > $min_release_ver)
      {
      if ($rel_version < $min_ver)
	{
	log_debug(" Rejecting $rel_version, less than what we need ($min_ver)");
	next RELEASE;
	}
      # reject pre-releases, unless we actually want one
      if ($release->{modversion} =~ /-pre\.[0-9]/ && $modinfo->{dependencies}->{$name} !~ /-pre\./)
	{
	log_debug(" Rejecting $rel_version, it is a pre-release version");
	next RELEASE;
	}
      log_debug(" Considering $name v$rel_version as usable.");
      $min_release_ver = $rel_version;
      $use_release = $release;
      }
    else
      {
      log_debug(" Rejecting $rel_version, less than what we already have ($min_release_ver)");
      }
    }

  if (!$use_release)
    {
    log_error("Found no usable release for $name");
    next;
    }

  my $fileid = $use_release->{fileid} // -1;
  my $filename = $use_release->{filename} // '';
  if ($fileid < 0)
    {
    log_error("Invalid fileid $fileid");
    next;
    }
  if ($destination)
    {
    log_debug("Downloading $name v$min_release_ver to $destination");
    my $rc = 1;
    if (FILE_CACHE_URL)
      {
      # try the cache first
      my $url = FILE_CACHE_URL . $fileid . '.zip';
      $rc = api_load_file($url, $destination, $filename);
      }
    if ($rc)
      {
      log_debug("Couldn't find the mod in the cache, trying the API");
      # and then the API if the cache failed
      my $url = API_FILE_URL . $fileid;
      $rc = api_load_file($url, $destination, $filename);
      }
    }
  else
    {
    # just output the final API file download URL
    my $url = API_FILE_URL . $fileid;
    print "$url\n";
    }
  }

#############################################################################
#############################################################################
# Subroutines

#############################################################################
# Load the info for one mod, returning either a reference or an error msg.

sub api_load_modinfo($)
  {
  my ($mod) = @_;

  my $url = API_BASE_URL . "mod/$mod";

  my $curl = WWW::Curl::Easy->new;

  #$curl->setopt(CURLOPT_HEADER,1);
  $curl->setopt(CURLOPT_NOPROGRESS,1);
  $curl->setopt(CURLOPT_URL, $url);
  $curl->setopt(CURLOPT_USERAGENT, USER_AGENT);

  my $response_body;
  $curl->setopt(CURLOPT_WRITEDATA, \$response_body);

  # Starts the actual request
  my $retcode = $curl->perform();

  # Parse the result as UTF_8 encoded JSON, or fail
  return parse_json($response_body) if $retcode == 0;

  my $response_code = $curl->getinfo(CURLINFO_HTTP_CODE);
  # Error code, type of error, error message
  "Error retrieving $url: $retcode " . $curl->strerror($retcode) . " " . $curl->errbuf;
  }

#############################################################################
# Download one file into the destination folder

sub api_load_file($ $ $)
  {
  my ($url, $destination, $filename) = @_;

  log_fatal("Invalid filename $filename") if $filename =~ /[\?\|\@\:\x00-\x1f<>\/\\"\*]/;

  my ($fh, $tmpname) = tempfile( 'VSMOD_XXXXXX', DIR => TEMP_DIR);

  log_fatal("Could not create temp file: $!")
  	unless $tmpname && -f $tmpname;

  my $curl = WWW::Curl::Easy->new;

  #$curl->setopt(CURLOPT_HEADER,1);
  $curl->setopt(CURLOPT_NOPROGRESS,1);
  $curl->setopt(CURLOPT_URL, $url);
  $curl->setopt(CURLOPT_USERAGENT, USER_AGENT);

  $curl->setopt(CURLOPT_WRITEDATA, $fh);

  log_debug("Downloading $url to $tmpname");

  # Starts the actual request
  my $retcode = $curl->perform();

  my $response_code = $curl->getinfo(CURLINFO_HTTP_CODE) // -1;

  close $fh;
  if ($retcode != 0 || $response_code != 200)
    {
    unlink $tmpname if -f $tmpname && DEBUG;
    my $err = $response_code == 404 ? "File not found." : $retcode . " " . $curl->strerror($retcode) . " " . $curl->errbuf();
    return "Error retrieving $url: $err";
    }

  # check that the result is a ZIP file:
  my $mimetype = `file -i -b '$tmpname'`;
  if ($mimetype !~ /^application\/zip/)
    {
    unlink $tmpname if -f $tmpname && DEBUG;
    return "File is not an ZIP archive, but $mimetype";
    }

  log_debug("Done. Moving file to '$destination/$filename'");

  # move the file and return the result
  my $rc = move ($tmpname, "$destination/$filename");

  log_fatal("Could not move $filename to $destination: $!") if $rc == 0;

  # Change the permissions, otherwise:
  #  -rw------- 1 root         root          243427 Mar 18 18:39 expanded_matter-2.5.1.zip
  # results in:
  # 18.3.2024 18:39:36 [Error] [expanded_matter-2.5.1.zip] Exception: Access to the path '/home/vintagestory/server/Mods/expanded_matter-2.5.1.zip' is denied.
  $rc = chmod 0644, "$destination/$filename";
  log_fatal("Could not chmod 0644, '$destination/$filename': $!") if $rc != 1;

  # everything is ok
  undef;
  }

#############################################################################
# Parse JSON data into a memory struct

sub parse_json($)
  {
  my ($data) = @_;

  # try to decode the data as UTF-8
  my $utf8;
  eval {
    $utf8 = encode( 'UTF-8', $data);
  };
  return "Could not decode data as UTF-8" unless $utf8;

  # try to parse the JSON
  my $json;
  eval {
    $json = decode_json($data);
  };
  $json // "Could not parse data as JSON";
  }

#############################################################################
# Routine to read and parse JSON from a file into memory in one go

sub parse_json_file($)
  {
  # load a file and parse it from JSON into memory
  my ($json_file) = @_;

  open (my $FH, '<', $json_file) or return undef;
  #  binmode $FH, ':utf8';
  local $/ = undef;	# slurp all data
  my $data = <$FH>;

  parse_json($data);
  }

#############################################################################
# Output some debug info

sub log_debug($)
  {
  my ($msg) = @_;

  print STDERR scalar localtime() . " DEBUG: $msg\n" if DEBUG;
  }

sub log_warn($)
  {
  my ($msg) = @_;

  warn( scalar localtime() . " DEBUG: $msg\n" );
  }

sub log_error($)
  {
  my ($msg) = @_;

  print STDERR scalar localtime() . " ERROR: $msg\n";
  exit(1) if DIE_ON_ERRORS;
  }

sub log_fatal($)
  {
  my ($msg) = @_;

  log_error($msg);
  exit(1);
  }

## END
