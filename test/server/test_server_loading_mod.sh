#!/bin/bash

# This script evaluates the server log and checks for problems while loading the mod.

# Example log entries this script is searching for:
#
#     21.1.2021 22:55:00 [Notification] Found 4 mods (0 disabled)
#     21.1.2021 22:55:00 [Notification] Mods, sorted by dependency: game, creative, survival, bricklayers
#     21.1.2021 22:55:09 [Warning] Failed resolving a blocks itemdrop or smeltedstack with code bricklayers:resin in cooking recipe bricklayers:recipes/cooking/glueportion.json
#     31.1.2021 22:55:20 [Event] Dedicated Server now running on Port 42420 and all ips!

MODNAME=`cat modinfo.json | jq .modid -r`

PREFIX='^[0-9\.\:_ -]+'

LOGFILE='/var/vintagestory/data/Logs/server-main.log'

# Wait for server to start
runtime="5 minute"
endtime=$(date -ud "$runtime" +%s)

server_startup_successful=false

while [[ $(date -u +%s) -le $endtime ]]
do
    # Check if server is up
    egrep "$PREFIX\[Event\] Dedicated Server now running on Port [0-9]+ and all ips" $LOGFILE

    if [ $? -eq 0 ]; then
        echo "Server startup finished"
        server_startup_successful=true
        break
    else
        echo "Server startup not finished yet, waiting for five seconds"
        sleep 5
    fi
done

if [ "$server_startup_successful" = false ]; then
    echo "Server did not start up within $runtime"
    exit 1
fi


# Line in log: 21.1.2021 22:55:00 [Notification] Found 4 mods (0 disabled)

# Check if all mods were loaded successfully
echo "Checking if all mods were loaded..."
egrep "$PREFIX\[Notification\] Found [0-9] mods \(0 disabled\)" $LOGFILE

if [ $? -eq 0 ]; then
    echo "All mods loaded"
else
    # If not, check if mods were loaded at all
    egrep "$PREFIX\[Notification\] Found [0-9] mods .*" $LOGFILE

    if [ $? -ne 0 ]; then
        echo "No mods loaded at all"
        exit 1
    fi

    echo "Some mods loaded, but with errors"
fi


# [Notification] Mods, sorted by dependency: game, creative, survival, bricklayers
# [Notification] Mods, sorted by dependency: game, creative, survival, em, tailorsdelight
# if dependencies are also loaded, the mod should be loaded last:
# [Notification] Mods, sorted by dependency: game, creative, survival, XYZ, OURMODIDHERE

# Check if all mods were loaded successfully
echo "Checking if all mods were loaded..."
egrep "$PREFIX\[Notification\] Mods, sorted by dependency: .*game, creative, survival(, [^,]+)*, $MODNAME" $LOGFILE

if [ $? -eq 0 ]; then
    echo "Mods loaded successfully"
else
    echo "Mods were not loaded successfull, here is the log message:"
    egrep " Mods, sorted by dependency: " $LOGFILE
    exit 1
fi


# Error in the mod:
# Line in log: 21.1.2021 22:55:09 [Warning] Failed resolving a blocks itemdrop or smeltedstack with code bricklayers:resin in cooking recipe bricklayers:recipes/cooking/glueportion.json
# Error in missing dependency:
# 18.3.2024 18:31:09 [Error] [OURMODIDHERE]     em@2.5.1 - Missing

# Check if there are warnings/errors regarding the mod
echo "Checking if there are warnings or errors regarding the mod..."
# Ignore some warnings about itemstack mapping
grep -e "$PREFIX\[(Warning|Error)\] .*$MODNAME.*" $LOGFILE | grep -v -e "Cannot fix itemstack mapping"

if [ $? -eq 0 ]; then
    echo "There were some warnings/errors regarding the mod."
    # Set to exit code 1 or think of a different solution on how to monitor warnings
    exit 1
fi

echo "There are no warnings/errors regarding the mod"
