#!/bin/bash

# A testfile - written as pure shell script.
#
# Output must be in the following format (X => number of tests)
#
# 1..X
# ok 1 - Name
# ok 2 - Other test
#
# Failing tests should create output like this:
# not ok 2 - Testname and reason for failure
#
# The "1..X" output might alternatively also appear at the end.
#
# This script verifies that all PNGs in the mod tree
# (assets and modinfo.png) have mimetype "image/png".
#
# Count all files and print "1..X"
echo "1..`find . -type f -name "*.png" | wc -l`"

# Print one ok/not ok line for each file
# find:  -print0 => use \x00 as line delimiter, so it works with file names with spaces
# sort:  -z	 => sort, and handle \x00 as delimiter
# xargs: -r 	 => do not run if empty list
#        -0 	 => accept \x00 as line delimiter
#        $fn 	 => filename
#        $mt 	 => mimetype
#        -IFOO	 => replace all occurances of FOO with the filename from find
# grep:  -o	 => output only the matching part
# file:  -b	 => do not append filename to output
find . -type f -name "*.png" -print0 | \
    sort -z | \
    xargs -r -0 -IFOO /bin/bash \
	-c 'fn="FOO" && mt=`file -i $fn | grep "image/png" -o || file -b -i $fn`; if [[ $mt = "image/png" ]]; then echo "ok - $fn is PNG"; else echo "not ok - $fn is not PNG but \"$mt\""; fi'
