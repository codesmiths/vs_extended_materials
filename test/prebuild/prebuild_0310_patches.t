#!/usr/bin/perl

# Test that JSON patches conform to some minimum standards:
#
# * contain valid JSON
# * contain a file: entry
# * have the right op and their matching fields

use Test::More;
use warnings;
use strict;
use utf8;

# load the test helper library
use lib 'test/lib';
use VSJSON;

# The global number of tests we did run
my $number_of_tests_run = 0;

use constant TEST_PATH => 'assets/';
use constant DEBUG => 0;

use constant QR_KNOWN_INGREDIENT_KEYS_GRID	=>
	qr/^(code|type|name|allowedVariants|skipVariants|attributes|quantity|toolDurabilityCost|isTool|returnedStack)$/;

# Step 1: Gather all JSON info
# Step 2: Test JSON patches

my $info = VSJSON::gather_json_info( TEST_PATH );
my $modid = $info->{modid};
like ($modid, qr/^[a-z]+[a-z0-9-]+$/, "modid $modid looks valid");
$number_of_tests_run ++;

# Test patches to be valid
for my $json_file (sort @{ $info->{patches} } )
  {
  my $json = VSJSON::parse_json($json_file);

  ok (ref($json), "$json_file has valid JSON");
  $number_of_tests_run ++;

  my $patches = ref($json) eq 'ARRAY' ? $json : [ $json ];

  my $patch_index = 0;
  # for all patches in this file
  for my $patch (@$patches)
    {
    $patch_index ++;

    my $name = "$json_file patch $patch_index";
    isnt ($patch->{file}, undef, "$name: has a file defined");

    # if the patch is in the compatibility folder, use the modid from the path
    my $othermod = $modid;
    $othermod = $1 if
        # assets/bricklayers/compatibility/moremetals/patches/ => moremetals
	$json_file =~ /assets\/[a-z0-9_-]+\/compatibility\/([a-z0-9_-]+)\/patches/
		or
	# to workaroun https://github.com/anegostudios/VintageStory-Issues/issues/2351
        # assets/bricklayers/patches/compatibility/moremetals/patches/ => moremetals
	$json_file =~ /assets\/[a-z0-9_-]+\/patches\/compatibility\/([a-z0-9_-]+)\//;

    # does a patch define extra namespaces?
    my $dep = join('|', @{ $patch->{compatibility} // [] } );

    like ($patch->{file}, $modid eq $othermod ? qr/^(game|$modid):/ : qr/^(game|$modid|$othermod|$dep):/, "$name: patches a valid namespace");

    like ($patch->{op}, qr/^(add|addeach|addmerge|move|remove|replace|test)$/, "$name: has a valid op");
    isnt ($patch->{path}, undef, "$name: defines a path");

    # make sure files mentioned ends in .json
    like ($patch->{file}, qr/\.json$/, "$name: file name ends in .json");

    my $op = $patch->{op} // '';
    if ($op =~ /^(add|addmerge|replace|test)$/)
      {
      ok (defined $patch->{value}, "$name: value must be defined");
      }
    elsif ($op eq 'move')
      {
      ok (defined $patch->{frompath}, "$name: defines a 'frompath' property");
      ok (!ref($patch->{frompath}), "$name: defines a scalar as 'frompath' property");
      ok (!exists $patch->{value}, "$name: does not define a value");
      $number_of_tests_run += 2;	# two extra tests for this op
      }
    elsif ($op eq 'remove')
      {
      ok (!exists $patch->{value}, "$name: does not define a value");
      }
    elsif ($op eq 'addeach')
      {
      is (ref($patch->{value}), 'ARRAY', "$name: value needs to be an ARRAY");
      }
    $number_of_tests_run += 6;

    # test that patches add sensible things to recipes
    if ($op =~ /^(add|addmerge|replace)$/)
      {
      my $value = $patch->{value} // '';
      if (ref($value) eq 'HASH' && exists $value->{isTool} && ref($value->{isTool}) ne 'JSON::PP::Boolean')
        {
	is (ref($value->{isTool}), 'JSON::PP::Boolean', "$name: ingredient $value->{code} needs isTool to be a boolan, not '1'");
	$number_of_tests_run ++;
	# check all keys to be valid
	my $ok = 0;
	for my $i_key (sort keys %$value)
          {
	  #print STDERR " check " . ($recipe->{name} // "undef") . " : $ingredient -> { $i_key }\n";
	  if ($i_key !~ QR_KNOWN_INGREDIENT_KEYS_GRID)
	    {
	    $number_of_tests_run ++;
	    $ok = 1;
	    ok (0, "$name has invalid key '$i_key'");
	    }
	  }
	$number_of_tests_run ++;
	is ($ok, 0, "patch value has only valid keys");
	}
      }
    }
  }

done_testing( $number_of_tests_run );

# End of code

