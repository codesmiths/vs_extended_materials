#!/usr/bin/perl

# Test that recipes conform to some minimum standards:
#
# * grid recipes: width/height should match the recipe pattern
# * output or returnedStack codes should only reference valid codes

use Test::More;
use warnings;
use strict;
use utf8;

# load the test helper library
use lib 'test/lib';
use VSJSON;

my $VERSION = '0.0.7';

# subroutine definitions
sub check_code($ $ $ $ $);
sub test_shape_name($ $ $ $ $ $);
sub test_shape($ $ $ $ $ $);

# The global number of tests we did run
my $number_of_tests_run = 0;

use constant TEST_PATH => 'assets/';
use constant DEBUG => 0;

use constant QR_KNOWN_RECIPE_KEYS_GRID		=>
	qr/^(attributes|code|comment|compatibility|copyAttributesFrom|enabled|width|height|ingredients|ingredientPattern|recipeGroup|requiresTrait|output|name|shapeless)$/;
use constant QR_KNOWN_INGREDIENT_KEYS_GRID	=>
	qr/^(code|type|name|allowedVariants|skipVariants|attributes|quantity|toolDurabilityCost|isTool|returnedStack)$/;
use constant QR_KNOWN_RECIPE_KEYS_BARREL	=>
	qr/^(code|comment|compatibility|name|enabled|sealHours|ingredients|recipeGroup|output)$/;
use constant QR_KNOWN_INGREDIENT_KEYS_BARREL	=>
	qr/^(code|type|name|allowedVariants|skipVariants|attributes|quantity|litres|consumeLitres|returnedStack)$/;
use constant QR_KNOWN_INGREDIENT_KEYS_COOKING	=>
	qr/^(code|validStacks|minQuantity|maxQuantity|portionSizeLitres)$/;

# Step 1: Gather all JSON info from blocktypes, itemtypes
# Step 2: Test recipes

my $info = VSJSON::gather_json_info( TEST_PATH );
my $modid = $info->{modid};
like ($modid, qr/^[a-z]+[a-z0-9-]+$/, "modid $modid looks valid");
$number_of_tests_run ++;

my $valid_codes = $info->{codes};		# contains all valid "base" codes like "glazedbricks"
my $valid_variants = $info->{variants};		# and all their variants like "glazedbricks-milky-red"

print "# Found " . (scalar keys %$valid_codes) . " codes and " . (scalar keys %$valid_variants) . " variants.\n";

# Test grid recipes for valid codes, and pattern size, sorted by name.
# Also build a list of all recipes on their ingredients sorted by code and quantity, so
# we can figure out double recipes:
#   "stick + knife + 2 twine"   => "stick:1|knife:1|twine:2"
#   "1 stick + knife + 2 twine" => "stick:1|knife:1|twine:2" (doubled recipe)
#   "knife + 1 twine + stick"   => "knife:1|twine:1|stick:1" (okay)
#   "knife + stick + 2 twine"   => "knife:1|stick:1|twine:2" (okay, different order)
#  We also take item/block into account, because technically, "item:stick" and "block:stick" could co-exist.
#  And we add the mod id, so "game:stick" and "modid:stick" differ, too.
# Shapeless recipes are not counted, as comparing these is way more complictated, they
# conflict easily and are to be avoided, anyway.

my $recipes_patterns = {};
for my $json_file (sort @{ $info->{grid_recipes} } )
  {
  my $json = VSJSON::parse_json($json_file);
  ok (ref($json), "$json_file has valid JSON");
  $number_of_tests_run ++;

  # #357: If the recipe is a single item, complain.
  is (ref($json), 'ARRAY', "$json_file: Recipes need to be a list, not a single item");
  $number_of_tests_run ++;

  last if ref($json) ne 'ARRAY';

  my $recipe_index = 0;
  # for all recipes in this file
  for my $recipe (@$json)
    {
    $recipe_index ++;
    my $name = defined $recipe->{code} ? "recipe $recipe->{code}" : defined $recipe->{name} ? "recipe $recipe->{name}" : "$json_file: recipe $recipe_index";
    $name .= ", pattern " . ($recipe->{ingredientPattern} // 'undef');
    # check that the recipe pattern matches height/width
    my $pattern = $recipe->{ingredientPattern};

    isnt ($pattern, undef, "$name: has a pattern defined");
    $number_of_tests_run ++;
    next unless defined $pattern;

    like ($pattern, qr/[A-Za-z_, ]+/, "$name: Pattern looks valid");
    $number_of_tests_run ++;

    # compute the height + width from the pattern
    my $height = 0;
    my $width = 0;
    my $first_width;
    # do all lines have the same width?
    my $all_the_same = 1;
    for my $line (split /,/, $pattern)
      {
      $height++;
      my $line_width = length($line);
      $width = $line_width if $line_width > $width;
      if (!defined $first_width)
        {
	$first_width = $width;
	}
      else
	{
        is ($line_width, $first_width, "Grid $name: Line $height must be $width characters");
        $number_of_tests_run ++;
	}
      }
    ok ($height >= 1 && $height <= 3 , "Grid $name: Pattern height looks sensible");
    ok ($width >= 1 && $width <= 3 , "Grid $name: Pattern width looks sensible");

    is ($height, $recipe->{height}, "Grid $name: Pattern height is correct");
    is ($width, $recipe->{width}, "Grid $name: Pattern width is correct");

    $number_of_tests_run += 4;

    # to build a unique pattern based on ingredients and quantity
    my @ingredients = ();

    my $pattern_checks = 0;
    # check that each recipe ingredient is actually defined
    my $char_index = 0;
    CHAR:
    for my $char (split (//, $pattern))
      {
      $char_index++;
      # ignore commas, underlines and spaces as separators
      next if $char =~ /[,_ ]/;

      # speed up testing by only running the test if it would fail
      if (!exists $recipe->{ingredients}->{ $char })
        {
        is (exists $recipe->{ingredients}->{ $char } ? 1 : 0, 1, "Grid $name: ingredient '$char' is defined");
        $number_of_tests_run ++;
	last CHAR;
	}
      if (ref($recipe->{ingredients}->{ $char }) ne 'HASH')
        {
        is (ref($recipe->{ingredients}->{ $char }), 'HASH', "Grid $name: ingredient '$char' is a HASH ref");
        $number_of_tests_run ++;
	last CHAR;
	}
      my $ing = $recipe->{ingredients}->{ $char };
      if (!defined $ing->{type} || $ing->{type} !~ /^(item|block)$/)
        {
        like ($ing->{type}, qr/^(item|block)$/, "Grid $name: ingredient '$char'->'type' is item or block");
        $number_of_tests_run ++;
	last CHAR;
	}
      if (!defined $ing->{code})
        {
        ok (defined $ing->{code}, "Grid $name: ingredient '$char'->'code' is defined");
        $number_of_tests_run ++;
	last CHAR;
	}

      # ignore this for shapeless recipes for now
      if (!$recipe->{shapeles})
        {
        my $code = $ing->{code}; $code = 'game:' unless $code =~ /^[a-z][a-z0-9_]+:/;	# add game: in case no domain exists
        # if this ingredient has attributes, add these
	# example: "attributes": { "type": "segment", "collected": true }
        if (ref($ing->{attributes}) eq 'HASH')
	  {
	  for my $key (sort keys %{$ing->{attributes}})
	    {
	    my $val = $ing->{attributes}->{$key} // 'undef';
	    $code .= ":att_$key=$val";
	    }
	  }

	# if the recipe has liquid attributes, add these to every code
	# example:  "attributes": { "liquidContainerProps": { "requiresContent": { "type": "item", "code": "game:dye-blue" }, "requiresLitres": 0.1 } },
	if ($recipe->{attributes} && ref($recipe->{attributes}->{liquidContainerProps}) eq 'HASH' &&
	  ref($recipe->{attributes}->{liquidContainerProps}->{requiresContent}))
	  {
	  # ignore litres, so we catch recipes that only differ in liter quantity
	  my $att = $recipe->{attributes}->{liquidContainerProps}->{requiresContent};
	  $code .= '::' . $att->{type} . ':' . $att->{code};
	  }

	my $vars = '*';
	if ($code =~ /\*/ && ref($ing->{allowedVariants}))
	  {
	  # replace "*" with a sorted variant list
	  $vars = join(",", sort @{ $ing->{allowedVariants} });
	  }
	elsif ($code =~ /\*/ && ref($ing->{skipVariants}))
	  {
	  # replace "*" with a sorted anti-variant list
	  my @anti = ();
	  for my $v (sort @{ $ing->{skipVariants} })
	    {
	    push @anti, "!$v";
	    }
	  $vars = join(",", @anti);
	  }
	$code =~ s/\*/$vars/;

        my $type = $ing->{type};
        my $quantity = $ing->{quantity} // 1;

        push @ingredients, "$char_index:$type:$code:$quantity";
        }

      $pattern_checks ++;
      }

    my $unique_pattern = join ("|", @ingredients);

    if (ref($recipes_patterns->{$unique_pattern}))
      {
      # use Data::Dumper; print Dumper ($recipes_patterns->{$unique_pattern} );
      my ($rfile, $rname) = @{ $recipes_patterns->{$unique_pattern} };
      ok (0, "Grid $name: $json_file: Recipe pattern '$unique_pattern' already seen in '$rfile' as recipe '$rname'");
      $number_of_tests_run ++;
      }
    # remember this instance
    $recipes_patterns->{$unique_pattern} = [ $json_file, $name ];

    # also check that no ingredients are present that are not in the pattern
    for my $char (keys %{ $recipe->{ingredients} })
      {
      if ($pattern !~ /$char/)
        {
	like ($pattern, qr/$char/, "Grid $name: ingredient '$char' appears in ingredientPattern");
	$pattern_checks ++;
	}
      }

    # 1.16/1.17: check that no ingredient defines liquid attributes, these should be on the recipe itself
    my $attribute_checks = 0;
    for my $code (keys %{ $recipe->{ingredients} })
      {
      my $thing = $recipe->{ingredients}->{$code};
      if (exists $thing->{attributes} && ref($thing->{attributes}) eq 'HASH')
	{
	if (exists $thing->{attributes}->{liquidContainerProps})
	  {
	  is (exists $thing->{attributes}->{liquidContainerProps} ? 1 : 0, 0, "Grid $name: ingredient '$code' should not have liquidContainerProps");
	  $attribute_checks ++;
	  }
	}
      }
    $number_of_tests_run += $attribute_checks;

    # test that recipes contain only know keys, so typos like "sealHourse" are avoided
    my $ok = 0;
    for my $key (sort keys %$recipe)
      {
      if ($key !~ QR_KNOWN_RECIPE_KEYS_GRID)
        {
        $number_of_tests_run ++;
	$ok = 1;
	ok (0, "$name has invalid key '$key'");
	}
      # check ingredients for valid keys
      if ($key eq 'ingredients')
        {
        for my $ingredient (sort keys %{$recipe->{ingredients}})
          {
          for my $i_key (sort keys %{$recipe->{ingredients}->{$ingredient}})
            {
		    #print STDERR " check " . ($recipe->{name} // "undef") . " : $ingredient -> { $i_key }\n";
	    if ($i_key !~ QR_KNOWN_INGREDIENT_KEYS_GRID)
	      {
	      $number_of_tests_run ++;
	      $ok = 1;
	      ok (0, "$name has invalid key '$i_key'");
	      }
	    }
	  }
	}
      }
    $number_of_tests_run ++;
    is ($ok, 0, "$name has only valid keys");
    }
  }

# test that barrel recipes contain only know keys, so typos like "sealHourse" are avoided
for my $json_file (sort @{ $info->{barrel_recipes} } )
  {
  my $json = VSJSON::parse_json($json_file);
  ok (ref($json), "$json_file has valid JSON");
  $number_of_tests_run ++;

  # #357: If the recipe is a single item, complain.
  is (ref($json), 'ARRAY', "$json_file: Recipes need to be a list, not a single item");
  $number_of_tests_run ++;

  last if ref($json) ne 'ARRAY';

  my $recipe_index = 0;
  # for all recipes in this file
  for my $recipe (@$json)
    {
    $recipe_index ++;
    my $name = defined $recipe->{code} ? "recipe $recipe->{code}" : defined $recipe->{name} ? "recipe $recipe->{name}" : "$json_file: recipe $recipe_index";

    my $ok = 0;
    for my $key (sort keys %$recipe)
      {
      if ($key !~ QR_KNOWN_RECIPE_KEYS_BARREL)
	{
	$number_of_tests_run ++;
	$ok = 1;
	ok (!exists $recipe->{$key}, "$name has invalid key '$key'");
	}
      # check ingredients for valid keys
      if ($key eq 'ingredients')
        {
        for my $ingredient (@{$recipe->{ingredients}})
          {
          for my $i_key (sort keys %$ingredient)
            {
		    #print STDERR " check " . ($recipe->{name} // "undef") . " : $ingredient -> { $i_key }\n";
	    if ($i_key !~ QR_KNOWN_INGREDIENT_KEYS_BARREL)
	      {
	      $number_of_tests_run ++;
	      $ok = 1;
	      ok (0, "$name has invalid key '$i_key'");
	      }
	    }
	  }
	}
      }
    $number_of_tests_run ++;
    is ($ok, 0, "$name has only valid keys");
    }
  } # end for all barrel recipes

# Now test all recipes for valid codes, sorted by name
FILE:
for my $json_file (sort @{ $info->{all_recipes} } )
  {
  my $json = VSJSON::parse_json($json_file);
  ok (ref($json), "$json_file has valid JSON");
  $number_of_tests_run ++;

  # skip further tests for legacy files with deprecated codes
  next FILE if $json_file =~ /legacy/;

  # Recipe can also be single recipes, convert them into a list
  my $recipes = ref($json) eq 'ARRAY' ? $json : [ $json ];

  # for all sub-recipes
  RECIPE:
  for my $recipe (sort @$recipes)
    {
    my $name = $recipe->{code} // 'unknown';

    # skip this test for cooking recipes
    #next if $json_file =~ /cooking/;

    my $output = $recipe->{output} // $recipe->{cooksInto};
    if (!ref($output) || ref($output) ne 'HASH')
      {
      is (ref($output), 'HAHS', "Found no output or cooksInto for recipe $name");
      next RECIPE;
      }

    # the output should not contain '*', but instead something like '{type}'
    if ($output->{code} =~ /\*/)
      {
      unlike ($output->{code}, qr/\*/, "Output of recipe $name does not contain '*'");
      $number_of_tests_run ++;
      }

    # does a recipe define extra namespaces?
    $info->{compatibility} = $recipe->{compatibility} // [];

    print STDOUT "Setting compatibility as '", join("', '", @{$info->{compatibility}}), "'\n"
	if scalar @{$info->{compatibility}} > 0;

    # check that output and returnedBlock match existing codes
    check_code( $info, $json_file, $name, $output->{code}, $output->{type} );

    # shape should only exist for cooking recipes
    if (exists $recipe->{cooksInto})
      {
      ok (exists $recipe->{shape}, 'Cooking recipe needs a shape');
      }
    else
      {
      ok (!exists $recipe->{shape}, 'Non-cooking recipe cannot have a shape');
      }
    $number_of_tests_run ++;

    # for cooking recipes
    if (exists $recipe->{shape})
      {
      # we expect no state variants here, so pass in []
      test_shape( \$number_of_tests_run, $info, $json_file, [], 'shape', $recipe->{shape} );
      }

    if (exists $recipe->{ingredients})
      {
      my $ingredients = $recipe->{ingredients};

      my @ingredient_list = ref($ingredients) eq 'HASH' ? sort keys %$ingredients : @$ingredients;
      for my $ingredient (@ingredient_list)
        {
	my $n = $name;
	my $ci = $ingredient;
	if (!ref($ingredient))
	  {
	  # for things like "M: { .. }"
	  $n = "$name ($ingredient)";
	  $ci = $ingredients->{$ci};
	  }
	$number_of_tests_run ++;
        next RECIPE unless is(ref($info->{compatibility}), 'ARRAY', 'recipe compatibility modid list needs to be an ARRAY');

	my @valid_stacks = ($ci);

	# if this is a cooking recipe, the ingredients are different
	#  "code": "wax", "validStacks": [ { "type": "item", "code": "game:beeswax" } ], "minQuantity": 3, "maxQuantity": 3
	if (exists $recipe->{cooksInto})
	  {
	  my $m = $ci->{code} // '(code is missing)';
	  $n = "$n (ingredient $m)";
	  # check that the ingredient contains only "code", "validStacks", "minQuantity", "maxQuantity", "portionSizeLitres etc.
	  for my $key (sort keys %$ci)
	    {
	    if ($key !~ QR_KNOWN_INGREDIENT_KEYS_COOKING)
	      {
	      $number_of_tests_run ++;
	      ok (!exists $ci->{$key}, "$n has invalid key '$key'");
	      next RECIPE;
	      }
	    }
	  # treat "validStats" as the ingredient. We verify that it only contains one entry for now
	  $ci = $ci->{validStacks};
	  if (!ref($ci) || ref($ci) ne 'ARRAY')
	    {
	    is (ref($ci), 'ARRAY', "$n validStacks is an ARRAY");
	    $number_of_tests_run ++;
	    next RECIPE;
	    }
	  # technically, we need to repeat the test for all in the list
	  if (@$ci == 0)
	    {
	    ok (@$ci > 0, "Expect exactly at least one entry in validStacks");
	    $number_of_tests_run ++;
	    next RECIPE;
	    }
	  # check all entries
	  @valid_stacks = @$ci;
	  }

	for my $i (@valid_stacks)
	  {
	  check_code( $info, $json_file, $n, $i->{code}, $i->{type} );
	  check_code( $info, $json_file, $n, $i->{returnedStack}->{code}, $i->{returnedStack}->{type} )
		if exists $i->{returnedStack} && ref($i->{returnedStack});

	  # check that if we have "allowedVariants", we also have name
	  if (exists $i->{allowedVariants})
	    {
	    if (!$i->{name})
	      {
	      ok (defined $i->{name}, "$json_file: Pattern $name: ingredient $i->{code} ($ingredient) with allowedVariants needs a 'name' set");
	      $number_of_tests_run ++;
	      }

	    if (ref($i->{allowedVariants}) ne 'ARRAY')
	      {
	      is (ref($i->{allowedVariants}), 'ARRAY', "$json_file: Pattern $name: ingredient $i->{code} ($ingredient): allowedVariants must be an ARRAY");
	      $number_of_tests_run ++;
	      }
	    else
	      {
	      # check that there are no duplicates in allowedVariants
	      my %names;
	      for my $variant (@{ $i->{allowedVariants} })
	        {
	        if (exists $names{$variant})
	          {
		  ok (!exists $names{$variant}, "$json_file: Pattern $name: ingredient $i->{code} ($ingredient): saw allowedVariants '$variant' twice");
		  $number_of_tests_run ++;
		  }
	        else
		  {
		  $names{$variant} = undef;	# remember this name for dupe check
		  }
		}
	      }
	    }

    	  # Make sure "isTool" is a boolean, and not 1
	  if (exists $i->{isTool} && ref($i->{isTool}) ne 'JSON::PP::Boolean')
	    {
	    is (ref($i->{isTool}), 'JSON::PP::Boolean', "$json_file: Pattern $name: ingredient $i->{code} ($ingredient) needs isTool to be a boolan, not '1'");
	    $number_of_tests_run ++;
	    }
	  # check that if we have "skipVariants", we also have name, otherwise the skipVariants is not applied
	  if (exists $i->{skipVariants})
	    {
	    if (!$i->{name})
	      {
	      ok (defined $i->{name}, "$json_file: Pattern $name: ingredient $i->{code} ($ingredient) with skipVariants needs a 'name' set");
	      $number_of_tests_run ++;
	      }
	    if (ref($i->{skipVariants}) ne 'ARRAY')
	      {
	      is (ref($i->{skipVariants}), 'ARRAY', "$json_file: Pattern $name: ingredient $i->{code} ($ingredient): skipVariants must be an ARRAY");
	      $number_of_tests_run ++;
	      }
	    else
	      {
	      # check that there are no duplicates in allowedVariants
	      my %names;
	      for my $variant (@{ $i->{skipVariants} })
	        {
	        if (exists $names{$variant})
	          {
		  ok (!exists $names{$variant}, "$json_file: Pattern $name: ingredient $i->{code} ($ingredient): saw skipVariants '$variant' twice");
		}
	        else
		  {
		  $names{$variant} = undef;	# remember this name for dupe check
		  }
		}
	      }
	    }
	  }
        } # end check for ingredient i

      # if we have attribues and liquidContainerProps on the recipe:
      if ($recipe->{attributes})
	{
	# example:
	#   "attributes": { "liquidContainerProps": { "requiresContent": { "type": "item", "code": "game:alcoholportion" }, "requiresLitres": 0.2 } },
	my $lprops = $recipe->{attributes}->{liquidContainerProps};
	if (ref($lprops))
	  {
	  is (ref($lprops), 'HASH', "$json_file: Pattern $name: recipe 'attributes->{liquidContainerProps} is not a HASH");

	  ok (exists $lprops->{requiresContent}, "$json_file: Pattern $name: recipe 'attributes->{liquidContainerProps}->{requiresContent} is not a HASH");
	  ok (exists $lprops->{requiresLitres}, "$json_file: Pattern $name: recipe 'attributes->{liquidContainerProps}->{requiresLitres} is not a HASH");
	  $number_of_tests_run += 3;

	  my $lcode = $lprops->{requiresContent}->{code} // 'not set';
	  my $ltype = $lprops->{requiresContent}->{type} // 'not set';

	  check_code( $info, $json_file, $name, $lcode, $ltype );
	  }
	}
      }
    else
      {
      # it must be a clay forming recipe
      ok (exists $recipe->{ingredient}, "Clayforming pattern $name is ok");
      $number_of_tests_run ++;
      }
    # reset extra namespaces for further tests
    $info->{compatibility} = [];
    }
  }

done_testing( $number_of_tests_run );

# End of code

#############################################################################
# Subroutines

sub check_code($ $ $ $ $)
  {
  # check that the given code is a valid one
  my ($info, $file, $name, $code, $type) = @_;

  # If the namespace of this code is something other than "$modid:" or "game:",
  # then check if the name space matches the other mod id in the file path
  # for compatibility recipes:
  # 'assets/bricklayers/compatibility/moremetals/recipes/' => $othermod => 'moremetals'
  my $othermod = undef;
  # Support also something like "Ceramos" or "my_mod_id"
  $othermod = lc($1) if $file =~ /assets\/[a-z0-9_]+\/compatibility\/([a-zA-Z0-9_-]+)\/(recipes|patches|compatibility)/;

  # #355 - compatibility recipes with other mods
  $info->{compatibility} = [] unless ref $info->{compatibility};
  if ($othermod)
    {
    # build a list of dependency mod ids
    my $dep = join('|', @{$info->{dependencies}}, @{$info->{compatibility}});

    # Only run this test if it would fail to speed things up
    if ($code !~ qr/^($dep|$othermod):/)
      {
      like ($code, qr/^($dep|$othermod):/, "$file: $code is in compatibility mod namespace");
      $number_of_tests_run ++;
      }

    # we cannot check the acual code in other mod namespaces, so skip the next test
    return;
    }

  my $rc = VSJSON::check_code($info, $file, $name, $code, $type);

  # only run this test if check_code failed to speed things up
  $number_of_tests_run ++, is ($rc, undef, "$file:$code is valid") if defined $rc;

  undef;
  }


sub test_shape_name($ $ $ $ $ $)
  {
  my ($tests, $info, $file, $states, $name, $code) = @_;

  # also allow references to mods we depend on
  like ($code, $info->{valid_codes}, "$file: ${name}'s shape reference $code has a valid modid");
  $$tests ++;

  # cannot test game: references yet
  return unless $code =~ /^$modid:/;

  # cannot test references with "{somestate}" in it yet
  return if $code =~ /\{/;

  # "modid:foo/bar" => "assets/modid/shapes/foo/bar.json"
  $code =~ s/^$modid:/assets\/$modid\/shapes\//;
  $code .= '.json';

  # check that "assets/$modid/shapes/$code" exists
  is (-f $code, 1, "Shape $code exists");
  $$tests ++;
  }

sub test_shape($ $ $ $ $ $)
  {
  my ($tests, $info, $file, $states, $name, $value) = @_;

  is (ref($value), 'HASH', "$file: $name is a reference");
  $$tests ++;

  #	"shapeByType": {
  #		"*-north": { "base": "game:block/clay/crock/base", "rotateY": 0 },
  #	"shapeinventoryByType": {
  #		"*-north": { "base": "game:block/clay/crock/base", "rotateY": 0 },
  # or:
  # 	"shape": { "base": "game:item/liquid" }

  # we expect only "base", or rotateX, rotateY, rotateZ
  my $found = 0;
  my $code = 'notfound';
  for my $key (sort keys %{$value})
    {
    $found ++ if $key =~ /^(base|rotate[XYZxyz]|scale|overlays|offset[XYZxyz])$/;
    $code = $value->{$key} if $key eq 'base';
    }
  my $cnt = scalar keys %$value;
  is ($found, $cnt, "$file: Shape reference $name has 'base' and only optional rotation or scale");
  if ($cnt != $found)
    {
      require Data::Dumper;
      print STDERR Data::Dumper::Dumper($value);
    }

  $$tests ++;

  test_shape_name( $tests, $info, $file, $states, $name, $code );
  }

