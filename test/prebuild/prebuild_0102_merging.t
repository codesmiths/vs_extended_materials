#!/usr/bin/perl

# Test that certain files have no left-over artifacts from failed merges

use Test::More;
use File::Find;
use warnings;
use strict;

my $number_of_tests_run = 0;

use constant TEST_PATH => '.';

my @FILES;

# define the subroutine to gather all files
sub find_file {
  push @FILES, $File::Find::name
	if -f $File::Find::name && $File::Find::name =~ /\.(json|md|sh|t|xml)\z/;
};

# Gather all files
find({ wanted => \&find_file, no_chdir => 1 }, TEST_PATH);

# test each file, sorted by name
for my $file (sort @FILES)
  {
  $number_of_tests_run ++;
  open (my $FH, '<', $file) or fail("$file - $!"), next;
  my $linenr = 0;
  my $fail = 0;
  # read the file line by line
  while (my $line = <$FH>)
    {
    $linenr ++;
    $fail = 1, last if $line =~ /^(>>>>>>> |>>>>>>> |=======\n)/;
    }
  if ($fail)
    {
    fail("$file - has left-over merge artifact in line $linenr");
    }
  else
    {
    ok(1, $file);
    }
  close $FH;
  }

done_testing( $number_of_tests_run );
