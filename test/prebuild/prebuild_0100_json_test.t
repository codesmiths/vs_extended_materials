#!/usr/bin/perl

# Test that all .json files are valid JSON

use Test::More;
use JSON;
use File::Find;
use warnings;
use strict;

my $number_of_tests_run = 0;

use constant TEST_PATH => 'assets/';

my @JSONS;

# also test that modinfo.json is valid JSON
push @JSONS, 'modinfo.json';

# define the subroutine to gather all JSON files
sub is_json {
  push @JSONS, $File::Find::name
	if -f $File::Find::name && $File::Find::name =~ /\.json\z/;
};

# Gather all files
find({ wanted => \&is_json, no_chdir => 1 }, TEST_PATH);

# test each file, sorted by name
for my $json_file (sort @JSONS)
  {
  $number_of_tests_run ++;
  open (my $FH, '<', $json_file) or fail("$json_file - $!"), next;
  local $/ = undef;	# slurp all data
  my $data = <$FH>;
  is (defined $data, 1, "$json_file has data");
  my $json;
  # try to parse the JSON
  eval {
    $json = decode_json($data);
  };
  $number_of_tests_run ++;
  ok( ref($json), "$json_file contains valid JSON");
  }

done_testing( $number_of_tests_run );
